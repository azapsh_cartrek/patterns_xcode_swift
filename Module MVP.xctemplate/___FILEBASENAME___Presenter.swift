//
//  ___FILENAME___
//
//  Created ___FULLUSERNAME___ on ___DATE___.

import Foundation

// MARK: View
protocol ___VARIABLE_productName:identifier___ViewProtocol: AnyObject {

}

// MARK: Presenter
protocol ___VARIABLE_productName:identifier___PresenterProtocol: AnyObject {
    func viewDidLoad()
}

class ___VARIABLE_productName:identifier___Presenter: ___VARIABLE_productName:identifier___PresenterProtocol {

    weak private var view: ___VARIABLE_productName: identifier___ViewProtocol?
    private var router: ___VARIABLE_productName: identifier___RouterProtocol

    init(view: ___VARIABLE_productName:identifier___ViewProtocol, router: ___VARIABLE_productName:identifier___RouterProtocol) {
        self.router = router
        self.view = view
    }
    
    func viewDidLoad() {

    }
}
