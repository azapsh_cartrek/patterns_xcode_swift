//
//  ___FILENAME___
//
//  Created ___FULLUSERNAME___ on ___DATE___.

import Foundation

// MARK: ViewModel
protocol ___VARIABLE_productName:identifier___ViewModelProtocol: AnyObject {
    func viewDidLoad()
}

class ___VARIABLE_productName:identifier___ViewModel: ___VARIABLE_productName:identifier___ViewModelProtocol {

    private var router: ___VARIABLE_productName: identifier___RouterProtocol

    init(router: ___VARIABLE_productName:identifier___RouterProtocol) {
        self.router = router
    }
    
    func viewDidLoad() {

    }
}
