//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created ___FULLUSERNAME___ on ___DATE___.

import UIKit

class ___VARIABLE_productName:identifier___ViewController: UIViewController {

	var viewModel: ___VARIABLE_productName: identifier___ViewModelProtocol?

	func setup(viewModel: ___VARIABLE_productName: identifier___ViewModelProtocol) {
        self.viewModel = viewModel
    }

	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel?.viewDidLoad()
    }
    
    override func loadView() {
        super.loadView()
        
    }
}
