//
//  ___FILENAME___
//
//  Created ___FULLUSERNAME___ on ___DATE___.

import UIKit

// MARK: Router
protocol ___VARIABLE_productName:identifier___RouterProtocol: AnyObject {

}

class ___VARIABLE_productName:identifier___Router: ___VARIABLE_productName: identifier___RouterProtocol {

    weak var view: UIViewController?

    init (view: UIViewController) {
        self.view = view
    }
}
